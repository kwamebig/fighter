import { showModal } from "./modal";

export function showWinnerModal(fighter) {
  // call showModal function 

  const winnerInfo = {
    title: 'Fight Is Over! All Hail',
    bodyElement: fighter.name.toUpperCase()
  };
  
  showModal(winnerInfo);
}